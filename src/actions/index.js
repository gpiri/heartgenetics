export const login = () => {
	return {
		type: "LOGIN",
	};
};

export const logout = () => {
	return {
		type: "LOGOUT",
	};
};

export const toggleLoginModal = () => {
	return {
		type: "TOGGLE_LOGIN_MODAL",
	};
};

export const openItemModal = (item) => {
	return {
		type: "OPEN_ITEM_MODAL",
		item,
	};
};

export const closeItemModal = () => {
	return {
		type: "CLOSE_ITEM_MODAL",
	};
};

export const addItems = (data) => {
	return {
		type: "ADD",
		data,
	};
};

export const removeItem = (id) => {
	return {
		type: "REMOVE",
		id,
	};
};

export const editItem = (newItem) => {
	return {
		type: "EDIT",
		id: newItem.id,
		data: newItem,
	};
};
