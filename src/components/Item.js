import React, { useState, useEffect } from "react";
import style from "./Item.module.css";
import { Modal } from "react-simple-hook-modal";
import "react-simple-hook-modal/dist/styles.css";
import { closeItemModal, editItem, removeItem } from "../actions";
import { useSelector, useDispatch } from "react-redux";

function Item() {
	const selectedItem = useSelector((state) => state.selectedItem);
	const dispatch = useDispatch();
	const [editMode, setEditMode] = useState(false);
	const [item, setItem] = useState(selectedItem);

	useEffect(() => {
		setItem(selectedItem);
	}, [selectedItem]);

	const handleSaveItem = () => {
		setEditMode(false);
		dispatch(editItem(item));
	};

	const handleDeleteItem = () => {
		dispatch(removeItem(item.id));
		dispatch(closeItemModal());
		setEditMode(false);
	};

	const handleClose = () => {
		setEditMode(false);
		dispatch(closeItemModal());
	};

	const handleChangeItemValue = ({ key, subKey, event }) => {
		const value = event.target.value;
		if (subKey) {
			setItem({ ...item, [key]: { ...item[key], [subKey]: value } });
		} else {
			setItem({ ...item, [key]: value });
		}
	};

	const col1 = [
		{ key: "name", label: "Name" },
		{ key: "username", label: "Username" },
		{ key: "email", label: "Email" },
	];

	const col2 = [
		{ key: "address", subKey: "street", label: "Street" },
		{ key: "address", subKey: "suite", label: "Suite" },
		{ key: "address", subKey: "city", label: "City" },
	];

	const customField = ({ key, subKey, label }) => (
		<div key={key} className={style.field}>
			<label for={key}>{label}</label>
			<input
				id={key}
				type="text"
				value={subKey ? item?.[key]?.[subKey] : item?.[key]}
				disabled={!editMode}
				onChange={(event) =>
					handleChangeItemValue({ key, subKey, event })
				}
			/>
		</div>
	);

	const Actions = () => {
		if (editMode) {
			return (
				<div className={style.actionsWrapper}>
					<button
						className={style.deleteButton}
						onClick={() => handleDeleteItem()}
					>
						Delete
					</button>
					<button
						className={style.saveButton}
						onClick={() => handleSaveItem()}
					>
						Save
					</button>
					<button
						className={style.cancelButton}
						onClick={() => setEditMode(false)}
					>
						Cancel
					</button>
				</div>
			);
		} else {
			return (
				<div className={style.actionsWrapper}>
					<button
						className={style.editButton}
						onClick={() => setEditMode(true)}
					>
						Edit
					</button>
					<button
						className={style.closeButton}
						onClick={() => handleClose()}
					>
						Close
					</button>
				</div>
			);
		}
	};

	return (
		<Modal
			id="loginModal"
			isOpen={selectedItem}
			onBackdropClick={() => handleClose()}
		>
			<div className={style.content}>
				<form>
					<div className={style.column}>
						<h2>Details</h2>
						{col1.map((field) => customField(field))}
					</div>
					<div className={style.column}>
						<h2>Address</h2>
						{col2.map((field) => customField(field))}
					</div>
				</form>
			</div>
			<Actions />
		</Modal>
	);
}

export default Item;
