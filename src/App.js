import React from "react";
import { useSelector } from "react-redux";
import Header from "./components/Header";
import Footer from "./components/Footer";
import LandingPage from "./components/LandingPage";
import List from "./components/List";
import Login from "./components/Login";
import { Modal } from "react-simple-hook-modal";
import "react-simple-hook-modal/dist/styles.css";
import { useDispatch } from "react-redux";
import { toggleLoginModal } from "./actions";

function App() {
	const isAuthenticated = useSelector((state) => state.isAuthenticated);
	const isLoginModalOpen = useSelector((state) => state.isLoginModalOpen);
	const dispatch = useDispatch();

	return (
		<div>
			<Header />
			<Modal
				id="loginModal"
				isOpen={isLoginModalOpen}
				onBackdropClick={() => dispatch(toggleLoginModal())}
			>
				<Login />
			</Modal>
			{isAuthenticated ? <List /> : <LandingPage />}
			<Footer />
		</div>
	);
}

export default App;
