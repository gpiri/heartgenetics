import React from "react";
import style from "./Footer.module.css";

function Footer() {
	return (
		<footer className={style.footer}>
			<p>This is a footer.</p>
		</footer>
	);
}

export default Footer;
