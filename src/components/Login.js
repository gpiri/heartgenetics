import React, { useState } from "react";
import style from "./Login.module.css";
import { BsX } from "react-icons/bs";
import { useDispatch } from "react-redux";
import { addItems, toggleLoginModal, login } from "../actions";

function Login() {
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [showErrorMessage, setShowErrorMessage] = useState(false);

	const dispatch = useDispatch();

	const validateEmail = () => {
		var re = /\S+@\S+\.\S+/;
		return re.test(email);
	};

	const validatePassword = () => {
		return password.length >= 8;
	};

	const fetchItems = () => {
		fetch("https://jsonplaceholder.typicode.com/users").then((response) => {
			if (response.ok) {
				response.json().then((data) => {
					console.log("DATA: ", data);
					dispatch(addItems(data));
				});
			}
		});
	};

	const handleLogin = (e) => {
		e.preventDefault();
		if (validateEmail() && validatePassword()) {
			setShowErrorMessage(false);
			dispatch(login());
			dispatch(toggleLoginModal());
			fetchItems();
		} else {
			setShowErrorMessage(true);
		}
	};

	return (
		<div className={style.loginModal}>
			<button
				className={style.closeButton}
				onClick={() => dispatch(toggleLoginModal())}
			>
				<BsX />
			</button>
			{showErrorMessage && (
				<p className={style.errorMessage}>
					Oops! Something went wrong.
				</p>
			)}
			<form>
				<div className={style.field}>
					<label for="email">Email</label>
					<input
						id="email"
						type="email"
						value={email}
						onChange={(e) => setEmail(e.target.value)}
					/>
				</div>
				<div className={style.field}>
					<label for="email">Password</label>
					<input
						id="password"
						type="password"
						value={password}
						onChange={(e) => setPassword(e.target.value)}
					/>
				</div>
				<button
					className={style.logInButton}
					onClick={(e) => handleLogin(e)}
				>
					Log In
				</button>
			</form>
		</div>
	);
}

export default Login;
