import React from "react";
import style from "./LandingPage.module.css";
import fillerImage from "../assets/images/filler-image.png";

function LandingPage() {
	return (
		<div className={style.wrapper}>
			<h1>Welcome</h1>
			<h2>
				This is the Landing Page for a simple React app made by Gonçalo
				Almeida
			</h2>
			<p>
				For the purpose of testing, given that there is no backend for
				the login functionality, a condition of a valid email and a
				password with a minimum of 8 characters was implemented just to
				emulate a simple error message.
			</p>
			<img src={fillerImage} alt="Filler" />
			<p>
				You can visit the real Heart Genetics website
				<a
					href="https://www.heartgenetics.com"
					target="_blank"
					rel="noopener noreferrer"
				>
					{" here "}
				</a>
				or visit Gonçalo's profile on LinkedIn
				<a
					href="https://www.linkedin.com/in/gpalmeida/"
					target="_blank"
					rel="noopener noreferrer"
				>
					{" here"}
				</a>
				.
			</p>
		</div>
	);
}

export default LandingPage;
