import React, { useState, useEffect } from "react";
import style from "./List.module.css";
import { BsTrash } from "react-icons/bs";
import { useSelector, useDispatch } from "react-redux";
import { removeItem, openItemModal } from "../actions";
import ReactTooltip from "react-tooltip";
import Item from "./Item";

const SMALL = 600;
const XSMALL = 425;

function List() {
	const [windowWidth, setWindowWidth] = useState(null);
	const items = useSelector((state) => state.items);
	const dispatch = useDispatch();

	const handleDeleteItem = (event, item) => {
		event.stopPropagation();
		dispatch(removeItem(item.id));
	};

	useEffect(() => {
		const handleResize = () => {
			setWindowWidth(window.innerWidth);
		};
		handleResize();
		window.addEventListener("resize", () => handleResize());
		return () => window.removeEventListener("resize", () => handleResize());
	}, []);

	return (
		<div className={style.listWrapper}>
			<Item />
			{items.length > 0 ? (
				<table>
					<tr className={style.headerRow}>
						<th>Name</th>
						{windowWidth > XSMALL && <th>Email</th>}
						{windowWidth > SMALL && <th>Address</th>}
						<th>Actions</th>
					</tr>
					{items.map((item) => {
						return (
							<tr
								key={item.id}
								className={style.row}
								onClick={() => dispatch(openItemModal(item))}
							>
								<td>{item.name}</td>
								{windowWidth > XSMALL && <td>{item.email}</td>}
								{windowWidth > SMALL && (
									<td>{`${item.address.street}, ${item.address.suite}, ${item.address.city}`}</td>
								)}
								<td>
									<div
										className={style.deleteButton}
										onClick={(e) =>
											handleDeleteItem(e, item)
										}
									>
										<BsTrash data-tip="Delete" />
										<ReactTooltip effect="solid" />
									</div>
								</td>
							</tr>
						);
					})}
				</table>
			) : (
				<h1>No Results</h1>
			)}
		</div>
	);
}

export default List;
