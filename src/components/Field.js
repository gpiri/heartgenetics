import React from "react";
import style from "./Field.module.css";

function Field(props) {
	const { id, label, onChange, type, value, disabled } = props;
	return (
		<div key={`${id}Field`} className={style.field}>
			<label for={id}>{label}</label>
			<input
				id={id}
				type={type || "text"}
				value={value}
				disabled={disabled}
				onChange={(e) => onChange(e)}
			/>
		</div>
	);
}

export default Field;
