import { combineReducers } from "redux";

// Ideally reducers would be in different files, imported here

const isAuthenticated = (state = false, action) => {
	switch (action.type) {
		case "LOGIN":
			return true;
		case "LOGOUT":
			return false;
		default:
			return state;
	}
};

const isLoginModalOpen = (state = false, action) => {
	switch (action.type) {
		case "TOGGLE_LOGIN_MODAL":
			return !state;
		default:
			return state;
	}
};

const items = (state = [], action) => {
	switch (action.type) {
		case "ADD":
			return action.data;
		case "REMOVE":
			return state.filter((x) => x.id !== action.id);
		case "EDIT":
			return state.map((item) => {
				return item.id === action.id ? action.data : item;
			});
		default:
			return state;
	}
};

const selectedItem = (state = null, action) => {
	switch (action.type) {
		case "OPEN_ITEM_MODAL":
			return action.item;
		case "CLOSE_ITEM_MODAL":
			return null;
		default:
			return state;
	}
};

const allReducers = combineReducers({
	isAuthenticated,
	isLoginModalOpen,
	items,
	selectedItem,
});

export default allReducers;
