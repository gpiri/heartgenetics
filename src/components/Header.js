import React from "react";
import logo from "../assets/images/logo.png";
import style from "./Header.module.css";
import { useSelector, useDispatch } from "react-redux";
import { logout, toggleLoginModal } from "../actions";

function Header() {
	const isAuthenticated = useSelector((state) => state.isAuthenticated);
	const dispatch = useDispatch();

	const handleLogInClick = () => {
		dispatch(toggleLoginModal());
	};

	const CTA = () => {
		if (isAuthenticated) {
			return (
				<button
					className={style.logoutButton}
					onClick={() => dispatch(logout())}
				>
					Log Out
				</button>
			);
		} else {
			return (
				<button
					className={style.loginButton}
					onClick={() => handleLogInClick()}
				>
					Log In
				</button>
			);
		}
	};

	return (
		<header className={style.header}>
			<img src={logo} className={style.logo} alt="logo" />
			<CTA />
		</header>
	);
}

export default Header;
